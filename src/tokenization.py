#!/usr/bin/python
# -*- coding: utf-8 -*-
import re


class Token(object):

  def __init__(
    self,
    type_id,
    line_number,
    column,
    text,
    ):
    self.type_id = type_id
    self.line_number = line_number
    self.column = column
    self.text = text

  def __str__(self):
    return text

  def __repr__(self):
    return 'Token(type_id = %s, line_number = %d, column = %d, text = %s)' % (repr(self.type_id), self.line_number,
          self.column, repr(self.text))


class Unmatched_text_error(Exception):

  def __init__(
    self,
    line_number,
    column,
    line,
    ):
    message = 'The text at line %d and column %d could not be tokenized' % (line_number, column)

    Exception.__init__(self, message)
    self.line_number = line_number
    self.column = column
    self.line = line


class Tokenizer(object):

  def __init__(self):
    self.rules = []

  def set_rules(self, rules):
    """
    Sets the rules (regular expressions) that will be used for tokenization of
    the source code.

    Args:
      rules (list): A list of tuples (token type id as string, regular expression as string)

    >>> tokenizer.set_rules([('assign', r'='), 
                             ('number', r'[0-9]')])

    .. note::
      The order **does** matter. 
      The only reserved token type id is 'empty', which stands for an empty
      line.
    """

    self.rules = []

    for (rule_number, rule) in enumerate(rules):
      (type_id, expression_str) = rule

      if type_id == 'empty':
        raise RuntimeError("The rule number %d was assigned a reserved token type id 'empty'." % rule_number)

      self.rules.append((type_id, re.compile(expression_str)))

  def tokenize(self, text):
    """
    Args:
      text (str): Text to be tokenized

    Returns:
      A list whose each item is a list of tokens
    """

    lines = text.split('\n')

    # delete trailing empty lines
    empty_line_re = re.compile(r'^( |\t)*$')
    first_nonempty_line = len(lines) - 1
    for (line_nr, line) in reversed(list(enumerate(lines))):
      if not empty_line_re.match(line):
        first_nonempty_line = line_nr
        break

    lines = lines[0:first_nonempty_line + 1]

    tokens = []

    for (line_number, line) in enumerate(lines):
      if len(line) == 0:
        tokens.append([Token('empty', 0, 0, '')])
        continue

      cursor = 0
      token_line = []

      while cursor < len(line):
        match = None

        for rule in self.rules:
          (type_id, regular_expression) = rule
          match = regular_expression.match(line, cursor)

          if match:
            token = Token(type_id, line_number, cursor, match.group(0))
            token_line.append(token)
            break

        if match == None:
          raise Unmatched_text_error(line_number, cursor, line)
        else:
          cursor = match.end(0)

      tokens.append(token_line)

    return tokens


