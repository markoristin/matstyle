function! Apply_matstyle()
  let line_number = line('.')
  let previous_content=join(getline(1,'$'), "\n")

  :execute "%!~/projects/matstyle/src/matstyle.py"

  let current_content=join(getline(1,'$'), "\n")

  if v:shell_error > 0
    :execute "%delete"

    let error_line_number = line_number
    if current_content =~ "^The file .* could not be tokenized at line [0-9]\\+" 
      let matches = matchlist(current_content, "^The file .* could not be tokenized at line \\([0-9]\\+\\)")
      let error_line_number = matches[1]
    endif

    :put =previous_content
    :normal gg
    :normal dd
    :execute ":".error_line_number
    ":normal j
    :put =current_content
    execute ":".error_line_number
  else
    :normal gg=G
    execute ":".line_number
  endif

endfunction
