#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re
from tokenization import Token, Tokenizer, Unmatched_text_error


def usage():
  print 'matstyle.py [path to matlab file]'
  print 'matstyle.py < [path to matlab file]'


def main():
  exit_code_usage = 1
  exit_code_unmatched = 2

  if len(sys.argv) == 1:
    src_path = 'STDIN'
    text = sys.stdin.read()
  elif len(sys.argv) == 2:
    src_path = sys.argv[1]

    fid = open(src_path, 'rt')
    text = fid.read()
    fid.close()
  else:
    usage()
    sys.exit(exit_code_usage)

  tokenizer = Tokenizer()
  tokenizer.set_rules([  # numbers
                         # varids
                         # comparisons
                         # operators
                         # assignment
                         # logical operators
                         # rest
    ('indent', r"^( |\t)+"),
    ('string', r"'([^']|'')*'"),
    ('int', r'[1-9][0-9]*|0'),
    ('float', r"([1-9][0-9]*|0)\.[0-9]+([eE][-+]?[0-9]+)?"),
    ('varid', r"[a-zA-Z_][a-zA-Z_0-9]*"),
    (r"<=", r"<="),
    (r"<", r"<"),
    (r">=", r">="),
    (r'>', r'>'),
    (r'~=', r'~='),
    (r"==", r"=="),
    ('+', r"\+"),
    (r"-", r"-"),
    ('*', r"\*"),
    (r"/", r"/"),
    ('^', r"\^"),
    (r"\\", r"\\"),
    ('./', r'\./'),
    ('.*', r'\.\*'),
    ('.^', r'\.\^'),
    (r"'", r"'"),
    ('~', r"\~"),
    (r"=", r"="),
    (r'&&', r'&&'),
    ('||', r'\|\|'),
    ('.', r"\."),
    (r";", r";"),
    (r":", r":"),
    ('comment', r"%.*$"),
    ('[', r"\["),
    (']', r"\]"),
    ('(', r"\("),
    (r')', r"\)"),
    (r",", r","),
    ('...', r"\.\.\."),
    (r"{", r"{"),
    (r"}", r"}"),
    (r" ", r" "),
    ])

  try:
    tokens = tokenizer.tokenize(text)
  except Unmatched_text_error, e:
    report = e.line[:e.column] + '<<<HERE>>>' + e.line[e.column:]

    print "The file '%(src_path)s' could not be tokenized at line %(line_number)d, column %(column_number)d: %(report)s" \
      % {
      'src_path': src_path,
      'line_number': e.line_number + 1,
      'column_number': e.column + 1,
      'report': report,
      }
    sys.exit(exit_code_unmatched)

  # render tokens

  binary_tokens = set([
    r"=",
    r"<=",
    r"<",
    r">=",
    r'>',
    r"==",
    r'~=',
    '+',
    r"-",
    '*',
    r"/",
    '^',
    '\\',
    './',
    '.*',
    '.^',
    ':',
    ])

  post_space_tokens = set([r",", r";", '(', '{', '['])
  pre_space_tokens = set([r')', '}', ']'])

  non_sign_indicators = set([r')', 'varid', r";", 'int', 'float'])

  output = []
  for token_line in tokens:
    nonspace_token_line_map = {}
    nonspace_token_line = []
    for (token_i, token) in enumerate(token_line):
      if token.type_id != r" " and token.type_id != 'indent':
        nonspace_token_line.append(token)
        nonspace_token_line_map[token_i] = len(nonspace_token_line) - 1

    output_line = []
    for (token_i, token) in enumerate(token_line):

      pre_nonspace_token = None
      post_nonspace_token = None
      if token_i in nonspace_token_line_map:
        nonspace_token_i = nonspace_token_line_map[token_i]
        if nonspace_token_i > 0:
          pre_nonspace_token = nonspace_token_line[nonspace_token_i - 1]

        if nonspace_token_i < len(nonspace_token_line) - 1:
          post_nonspace_token = nonspace_token_line[nonspace_token_i + 1]

      if token.type_id in ['+', r"-"] and (pre_nonspace_token == None or pre_nonspace_token.type_id
                                           not in non_sign_indicators):
        output_line.append(token.text)

      elif token.type_id in binary_tokens:

        # add preceding space
        if token_i > 0:
          if token_line[token_i - 1].type_id != r" ":
            if output_line[-1] != ' ':
              output_line.append(r" ")

        output_line.append(token.text)

        # add following space
        if token_i < len(token_line) - 1:
          if token_line[token_i + 1].type_id != r" ":
            if output_line[-1] != ' ':
              output_line.append(r" ")
      elif token.type_id in post_space_tokens:

        # add space after
        output_line.append(token.text)
        if token_i < len(token_line) - 1:
          if token_line[token_i + 1].type_id != r" ":
            if output_line[-1] != ' ':
              output_line.append(r" ")
      elif token.type_id in pre_space_tokens:

        # add space before
        if token_i > 0 and token_line[token_i - 1].type_id != r" ":
          if output_line[-1] != ' ':
            output_line.append(r" ")
        output_line.append(token.text)
      else:

        output_line.append(token.text)

    output.append(''.join(output_line))

  sys.stdout.write('\n'.join(output))


  # print '\n'.join(output)

if __name__ == '__main__':
  main()
