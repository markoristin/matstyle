# README #

### Installation ###

Make sure you have a python up and running. Just clone the code and you are all set up. You can add *src* directory to your path variable for convenience.

### Usage ###


```
#!bash

$ matstyle [path to the file with matlab code]
```
or alternatively
```
#!bash

$ matstyle < [path to the file with matlab code]
```


The script will output the beautified source code to STDOUT.

### Indention ###

The script does not handle indention yet, since VIM accomplishes it in a satisfying manner. Please let me know if that feature would be desirable.

### Integration with VIM ###
In order to integrate the script with VIM add the function defined in *src/apply_matstyle.vim* to ~/.vim/ftplugin/matlab.vim file (please make sure to update the path to matstyle script to match your setting). 

To beautify the code, invoke:
```
#!vim
:call Apply_matstyle()
```